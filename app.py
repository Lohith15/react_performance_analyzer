from flask import Flask, request, jsonify
#from flask_cors import CORS
from backanalyze import *

app = Flask(__name__)
#CORS(app)

@app.route('/api/output1', methods=['POST'])
def get_output1():
    input_value = request.json['input']
    output_value=ask_question(input_value)
    return jsonify({'output': output_value})

@app.route('/api/output2', methods=['POST'])
def get_output2():
    input1 = request.json['input1']
    input2 = request.json['input2']
    input3 = request.json['input3']
    output_value=check_answer(input2,input1,input3)
    return jsonify({'output': output_value})


if __name__ == '__main__':
    app.run()
