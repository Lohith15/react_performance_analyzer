import React, { useState } from "react";
import axios from "axios";
import "./App.css";
//import logo from "./pwc logo.png";

const App = () => {
  const [inputValue1, setInputValue1] = useState("");
  const [outputValue1, setOutputValue1] = useState("");
  const [inputValue2, setInputValue2] = useState("");
  const [outputValue2, setOutputValue2] = useState("");

  const handleSubmit1 = (e) => {
    e.preventDefault();
    axios
      .post("/api/output1", { input: inputValue1 })
      .then((response) => {
        setOutputValue1(response.data.output);
      })
      .catch((error) => {
        console.error(error);
      });
  };

  const handleSubmit2 = (e) => {
    e.preventDefault();
    axios
      .post("/api/output2", { input1: outputValue1, input2: inputValue2 ,input3: inputValue1})
      .then((response) => {
        setOutputValue2(response.data.output);
      })
      .catch((error) => {
        console.error(error);
      });
  };

  return (
    <div className="container">

      <div className="header">
       <h1 className="title">Performance Analyzer</h1>
      </div>

      <div className="form-container">
        <form onSubmit={handleSubmit1}>
          <div className="input-container">
            <input
              type="text"
              value={inputValue1}
              onChange={(e) => setInputValue1(e.target.value)}
              placeholder="Enter Topic Here.."
            />
            <button type="submit">Ask</button>
          </div>
        </form>
        <div className="output-container">
          <p>Question :</p>
          <div className="output">{outputValue1}</div>
        </div>
      </div>

      <div className="form-container">
        <form onSubmit={handleSubmit2}>
          <div className="input-container">
            <input
              type="text"
              value={inputValue2}
              onChange={(e) => setInputValue2(e.target.value)}
              placeholder="Answer Here.."
            />
            <button type="submit">Analyze</button>
          </div>
        </form>
        <div className="output-container">
          <p>Analyzed Feedback:</p>
          <div className="output">{outputValue2}</div>
        </div>
      </div>
    </div>
  );
};

export default App;