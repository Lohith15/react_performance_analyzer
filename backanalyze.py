import tiktoken
import openai
import os
openai.api_type = "azure"
openai.api_version = "2023-05-15" 
openai.api_base = 'https://lohithopenai.openai.azure.com/' 
openai.api_key = '5acffb6b2da94a1ead6d3406a85cc06d'


GeneratedQuestions=[]
def ask_question(Topic): 
    Var=f"Different and unique Question on subject or topic or technology of {Topic}"
    system_message = [{"role": "system", 
    "content": f"""You are an Intelligent Questions Asking System designed to Ask Technical Questions On Concept or Topic or Technology or Subject on {Topic}.
    Instructions to Ask The Question:
    - You should not Repeat the Questions Once you asked.
    - You should Ask Different Questions Each time.
    - You should ask questions for Intermediate Level User.
    - Each time you have to ask Single Question.
    """
    },
    {"role": "user", "content": f"Generate a {Var}. You should ask only one Question. Also Strictly you should Generate Different Questions.You should not generate Questons that is already present in this list:{GeneratedQuestions}"},
    {"role": "assistant","content":f"What is {Topic}?"}
    ]
    
    max_response_tokens = 250  
    response = openai.ChatCompletion.create(
        engine="gpt-35-turbo", 
        messages = system_message,
        temperature=.7,
        max_tokens=max_response_tokens,
    )

    GeneratedQuestions.append(response['choices'][0]['message']['content'])
    if(len(GeneratedQuestions)>1000):
        GeneratedQuestions.clear()
    return response['choices'][-1]['message']['content']







def check_answer(ans,ques,topic): 
    system_message = {"role": "system", "content": 
                      
                      f"""
                      Check this Answer : {ans} is Correct or Related to the Actual answer for this Question : {ques}. This Question and Answer is releated to topic/Subject/Technology of :{topic}
                      If You think the Answer for the above Question is Wrong Then Strictly give 'Wrong Answer' as Output.Along with it also give a brief explaination why it is Correct according to you.
                      If You think the Answer for the above Question is Partial Correct Then Strictly give 'Partial Correct' as output.Along with it also give a brief explaination why it is Partial Correct according to you.
                      If You think the Answer for the above Question is Right then Strictly give 'Right Answer' as Output.Along with it also  also give a brief explaination why it is Wrong according to you.
                      You have to give Output as fast as Possible.
                      """
                      }
    max_response_tokens = 250
    conversation=[]
    conversation.append(system_message)
    conversation.append({"role": "user", "content": f"Check This Answer :{ans} is correct for this Question: {ques} related to Topic/Subject/Technology: {topic}."})
     
    response = openai.ChatCompletion.create(
        engine="gpt-35-turbo",
        messages = conversation,
        temperature=.7,
        max_tokens=max_response_tokens,
    )

    conversation.append({"role": "assistant", "content": response['choices'][0]['message']['content']})
    return response['choices'][-1]['message']['content']
